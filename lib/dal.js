let responses = require("./responses");
let Promise = require("bluebird");
let logger = require("./logger")

let readOnlyConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT
};

let writeOnlyConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT
};

let pgPool = require('pg').Pool;
let readOnlyConnection = new pgPool(readOnlyConfig);
let writeOnlyConnection = new pgPool(writeOnlyConfig);

let Dal = function ()
{

};

Dal.prototype.executeReadCommand = function (sql, values)
{
    var logSession = logger.getLogSession('debug', 'dal', 'executeReadCommand');
    logSession.log("execute query");
    logSession.log(sql);

    return readOnlyConnection.query(sql, values)
        .then(function (result)
        {
            logSession.log("query executed");
            var response = normalizeResults(result);
            return response;
        }).catch(function (pgError)
        {
            logSession.log(`error executing query ${JSON.stringify(pgError)}`);
            var response = responses.errorResponse();
            response.status.description = pgError.message;
            response.status.stackTrace = pgError.stack;
            return response;
        });
}

Dal.prototype.executeWriteCommand = function (sql, values, analytics = false)
{
    var logSession = logger.getLogSession('debug', 'dal', 'executeWriteCommand');
    logSession.log("execute write query");
    logSession.log(sql);

    var connection = writeOnlyConnection;
    if (analytics === true)
    {
        connection = analyticsConnection;
    }

    return connection.query(sql, values)
        .then(function (result)
        {
            if (result && result.command === 'UPDATE' && result.rowCount === 0)
            {
                logSession.log("write query returned without any updated rows", error);
                var response = responses.errorResponse();
                response.status.description = 'Update failed, no rows updated';
                return response;
            }
            else if (result && result.command === 'INSERT' && result.rows.length > 0)
            {
                // https://stackoverflow.com/a/2944481
                // possible to return id after insert
                var response = responses.successResponse();
                return Object.assign({}, response, result.rows[0]);
            }
            else 
            {
                logSession.log("write query executed");
                var response = responses.successResponse();
                return response;
            }
        }).catch(function (pgError)
        {
            logSession.log(`error executing write query ${JSON.stringify(pgError)}`);
            var response = responses.errorResponse();
            response.status.description = pgError.message;
            response.status.stackTrace = pgError.stack;
            return response;
        });
}

function normalizeResults(result)
{
    var response = responses.errorResponse();
    response.status.description = "Unable to read the query results.";

    if (result != undefined && result.rows != undefined)
    {
        response = responses.successResponse();
        response.rows = result.rows;
    }
    return response;
}

module.exports = new Dal();