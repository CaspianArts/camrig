//application logging
const winston = require('winston');
const helper = require('./helper');

module.exports.LOGGER_TAG_ENTRY = "ENTRY";
module.exports.LOGGER_TAG_EXIT = "EXIT";
module.exports.LOGGER_TAG_FAIL_EXIT = "FAIL_EXIT";

winston.configure({
    transports: [
        new (winston.transports.Console)({
            level: process.env.LOG_LEVEL,
            colorize: false,
            timestamp: false,
            json: false,
            stringify: false,
            prettyPrint: false,
            depth: 5,
            humanReadableUnhandledException: true,
            showLevel: false,
            stderrLevels: ['error'], //notice 'debug' is removed from the default configuration
        })
    ]
});

const uuidV4 = require('uuid/v4');

let Logger = function ()
{

};

Logger.prototype.getLogSession = function (logLevel, moduleName, methodName, metadata)
{
    var currentTime = new Date();
    var logSession = {
        logSessionId: uuidV4(),
        startTime: currentTime,
        lastLogTime: currentTime,
        logLevel: logLevel,
        moduleName: moduleName,
        methodName: methodName
    };

    if (helper.isValid(metadata) === true)
    {
        //merge metaData properties into the logSession object
        Object.assign(logSession, metadata);
    }

    logSession.start = function start(props)
    {
        var startProps =
            {
                message: exports.LOGGER_TAG_ENTRY
            }

        if (helper.isValid(props) === true)
        {
            if (typeof props === 'string')
            {
                startProps.metadata = props;
            }
            else
            {
                //don't let anyone override our message, and let's 
                //preserve theirs
                if (props.hasOwnProperty('message') == true)
                {
                    if (helper.isValid(props.message) == true)
                    {
                        props.messageExtra = props.message;
                    }
                    delete props.message;
                }
                Object.assign(startProps, props);
            }
        }

        this.log(startProps);
    }

    logSession.stop = function stop(props)
    {
        var stopProps =
            {
                message: exports.LOGGER_TAG_EXIT
            }

        if (helper.isValid(props) === true)
        {
            if (typeof props === 'string')
            {
                stopProps.metadata = props;
            }
            else
            {
                //don't let anyone override our message, and let's 
                //preserve theirs
                if (props.hasOwnProperty('message') == true)
                {
                    if (helper.isValid(props.message) == true)
                    {
                        props.messageExtra = props.message;
                    }
                    delete props.message;
                }
                Object.assign(stopProps, props);
            }
        }

        this.log(stopProps);
    }

    logSession.fail = function fail(reason)
    {
        if (typeof reason !== 'string')
        {
            reason = JSON.stringify(reason);
        }

        this.log({ message: exports.LOGGER_TAG_FAIL_EXIT, reason: reason });
    }

    logSession.log = function log(message, logLevel)
    {
        var useLogLevel = this.logLevel;
        if (logLevel !== undefined)
        {
            useLogLevel = logLevel;
        }

        var currentTime = new Date();
        var runningMillis = currentTime - this.startTime;
        var lastLogMillis = currentTime - this.lastLogTime;
        this.lastLogTime = currentTime;

        var timestamp = currentTime.getTime();

        var logEntry = {
            logSessionId: this.logSessionId,
            timestamp: timestamp,
            moduleName: this.moduleName,
            methodName: this.methodName,
            runningMillis: runningMillis,
            lastLogMillis: lastLogMillis,
            logLevel: useLogLevel
        }

        if (typeof message !== 'string')
        {
            Object.assign(logEntry, message);
        }
        else
        {
            logEntry.message = message;
        }
        /*
        silly: winston.LeveledLogMethod;
        debug: winston.LeveledLogMethod;
        verbose: winston.LeveledLogMethod;
        info: winston.LeveledLogMethod;
        warn: winston.LeveledLogMethod;
        error: winston.LeveledLogMethod;
        */

        var message = JSON.stringify(logEntry);
        winston.log(useLogLevel, message);
    }

    return logSession;
}

module.exports = new Logger;