let helper = function ()
{

};

helper.prototype.isValid = function (value)
{
    return ((undefined != typeof value) && (null != value));
};

helper.prototype.isValidObject = function (obj)
{
    var h = new helper();
    return h.isValid(obj) && ("object" === typeof obj) && !(obj instanceof Array);
};

helper.prototype.isValidString = function (strValue)
{
    var h = new helper();
    if ("string" === typeof strValue && h.isValid(strValue))
    {
        return true;
    }
    return false;
};

helper.prototype.isStringEmpty = function (strValue)
{
    if (undefined === strValue || null === strValue || "" === strValue)
    {
        return true;
    }
    return false;
};

helper.prototype.isValidUuid = function (strValue)
{

    if (strValue && /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(strValue))
    {
        return true;
    }
    else
    {
        return false;
    }
}

helper.prototype.isEmailAddress = function (email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

helper.prototype.buildOrderByClause = function (field, order)
{
    return "ORDER BY " + field + ' ' + order;
}

/**
 * Function will create a new object with all the keys in camel case.
 * Note that if the object has nested object those objects will remain unchanged
 * only making alteration to the first level of the object.
 * @param {object} snakeCaseObject     - object with snake case keys.
 * @returns {object}                   - object with the camel case keys
 */
helper.prototype.snakeCaseToCamalCaseObject = function (snakeCaseObject)
{
    var camelCaseObject = {};

    var replaceFun      = function (token)
    {
        return token[1].toUpperCase();
    };

    for (var key in snakeCaseObject)
    {
        var camelCaseKey = key.replace(/_\w/g, replaceFun);
        camelCaseObject[camelCaseKey] = snakeCaseObject[key];
    }
    return camelCaseObject
};

helper.prototype.normalizeToScale = function(val, a, b, c, d)
{
    var newVal = 0.0;

    //Old Scale: a - b
    //New Scale: c - d

    newVal = c + (val - a) * (d - c) / (b - a);

    return newVal;
}

module.exports = new helper;