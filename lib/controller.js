'use strict'

const Gpio = require('onoff').Gpio;
const sleep = require('sleep');
const responses = require('./responses');
const helper = require('./helper')

//forward, by default, away from motor
//reverse, by default, toward motor
const Direction = { 'Forward': 0, 'Reverse': 1 };
Object.freeze(Direction);

const Mode = { 'Normal': 0, 'Calibrate': 1, 'CalibrateOverride': 2 };
Object.freeze(Mode);

var Controller = function ()
{

}

Controller.prototype.initialize = function (config)
{
    if (config.devices !== undefined)
    {
        console.log(`device dump...`);
        console.log(`${JSON.stringify(config.devices)}`);

        this.devices = config.devices;
        this.devices.map((device) =>
        {
            device.currentPosition = 0;
            device.currentDirection = device.forwardDirection;
            device.limit = false;
            device.linearLength = 1000; //arbitrarily give 1000mm
            device.totalPulses = 0;
            device.mode = Mode.Normal;

            console.log(`${device.name}, pulse pin ${device.pulsePin}, dir pin ${device.dirPin}, limit pin ${device.limitPin}, ppr ${device.pulsesPerRevolution}, mmpr ${device.millimetersPerRevolution}, pulse sleep microseconds: ${device.pulseSleepMicroseconds}`);

            if (device.pulsePin !== undefined)
            {
                device.pulseOut = new Gpio(device.pulsePin, 'out');
            }
            if (device.dirPin !== undefined)
            {
                device.dirOut = new Gpio(device.dirPin, 'out');
                device.dirOut.writeSync(device.currentDirection);
            }
            if (device.limitPin !== undefined)
            {
                console.log(`limit pin found on ${device.name} on pin ${device.limitPin}`);
                device.limitIn = new Gpio(device.limitPin, 'in', 'both');
                console.log(`GPIO limit is ${device.limitIn}`);
                device.limitIn.watch((err, val) => 
                {
                    if (val > 0)
                    {
                        console.log(`********limit tripped for ${device.name} with val ${val} and err ${err}`);
                    }
                });
            }

            this.readDeviceState(device.name);
        })
    }
}

Controller.prototype.calibrate = function (deviceName)
{
    console.log('start calibrating');
    var device = this.getDevice(deviceName)

    if (device !== undefined)
    {
        console.log(`calibrating ${device.name}`);
        //dangerous exercise if limit switch is not on...
        if (helper.isValid(device.limitIn) === true)
        {
            console.log('begin calibration now');

            //back to home limit switch and set position = 0
            device.mode = Mode.Calibrate;
            device.currentDirection = device.reverseDirection;
            device.dirOut.writeSync(device.reverseDirection);
            this.pulseMotor(device.name, Number.MAX_VALUE);

            console.log('home is found');

            //clear the limit switch
            device.currentDirection = device.forwardDirection;
            device.dirOut.writeSync(device.forwardDirection);

            console.log('begin pulse out...');

            device.mode = Mode.CalibrateOverride;

            //pulse until limit switch is not pressed
            var limitValue = device.limitIn.readSync()
            while (limitValue !== 0)
            {
                this.pulseMotor(device.name, 1);
                limitValue = device.limitIn.readSync();
                console.log('pulsing out of limit...', limitValue);
                sleep.msleep(1);
            }

            device.mode = Mode.Calibrate;

            console.log('set home to zero');

            device.currentPosition = 0;

            console.log('device.limitIn.readSync()', device.limitIn.readSync());

            console.log('move forward and find linear length');
            //now move forward and find linear length
            let totalPulses = this.pulseMotor(device.name, Number.MAX_VALUE);
            console.log('total pulses ' + totalPulses);

            let totalRevolutions = totalPulses / device.pulsesPerRevolution;
            console.log('total revolutions ' + totalRevolutions);

            //linear length in millimeters
            device.linearLength = totalRevolutions * device.millimetersPerRevolution;

            console.log('linear length is ' + device.linearLength + 'mm');

            device.currentDirection = device.reverseDirection;
            device.dirOut.writeSync(device.reverseDirection);

            console.log('begin pulse outi to go home...');

            device.mode = Mode.CalibrateOverride;

            //pulse until limit switch is not pressed
            var limitValue = device.limitIn.readSync()
            while (limitValue !== 0)
            {
                this.pulseMotor(device.name, 1);
                limitValue = device.limitIn.readSync();
                console.log('pulsing out of limit to go home...', limitValue);
                sleep.msleep(1);
            }

            //place back in normal mode
            device.mode = Mode.Normal;

            this.setPosition(device.name, 0);
        }
        else
        {
            console.log('limit switch is not defined');
        }
    }
    return responses.successResponsePromise();
};

Controller.prototype.calibrationMove = function (deviceName, distance, direction)
{
    return new Promise(resolve =>
    {
        console.log('start calibrating');
        var device = this.getDevice(deviceName)

        if (device !== undefined)
        {
            //back to home limit switch and set position = 0
            device.mode = Mode.Calibrate;
            this.move(deviceName, distance, direction);
            device.mode = Mode.Normal;
        }

        resolve(response.successResponse());
    });
}

//distance in millimeters
Controller.prototype.move = function (deviceName, distance, direction)
{
    var device = this.getDevice(deviceName)

    console.log(`moving device ${deviceName} distance ${distance} direction ${direction}`);

    if (helper.isValid(device) === true && helper.isValid(direction) === true)
    {
        //how many revolutions are required to meet the requested distance?
        var revolutions = distance / device.millimetersPerRevolution;
        //how many pulses required to meet the revolutions
        var pulses = revolutions * device.pulsesPerRevolution;

        console.log(distance, direction, device.millimetersPerRevolution, device.pulsesPerRevolution, revolutions, pulses, `${deviceName} pulsing starts now...`);

        device.currentDirection = direction;
        device.dirOut.writeSync(device.currentDirection);

        this.pulseMotor(device.name, pulses);
    }

    return responses.successResponsePromise();
}

//distance in millimeters
Controller.prototype.setPosition = function (deviceName, position)
{
    var device = this.getDevice(deviceName)

    console.log(`positioning device ${deviceName}`);

    if (device !== undefined)
    {
        console.log('we found the device and current position is', device.currentPosition);
        const distance = position - device.currentPosition;
        if (distance !== 0)
        {
            console.log(`default to forward direction ${device.forwardDirection}`);
            var direction = device.forwardDirection;
            if (distance < 0)
            {
                console.log(`distance is less than zero, move in reverse direction ${device.reverseDirection}`);
                direction = device.reverseDirection
            }

            this.move(deviceName, Math.abs(distance), direction);
        }
    }

    return responses.successResponsePromise();
};

Controller.prototype.setRotation = function (deviceName, rotation)
{
    var device = this.getDevice(deviceName)

    console.log(`rotating device ${deviceName} to ${rotation} degrees`);

    if (device !== undefined)
    {
        //where do we want to be? in mm...
        const position = helper.normalizeToScale(rotation, 0, 360, 0, device.millimetersPerRevolution);
        const distance = position - device.currentPosition;

        console.log(`translated distance is ${position} and delta distance will be ${distance}`);

        if (distance !== 0)
        {
            var direction = device.forwardDirection;
            if (distance < 0)
            {
                direction = device.reverseDirection;
            }

            this.move(deviceName, Math.abs(distance), direction);
        }
    }

    return responses.successResponsePromise();
};

//the purpose is primarily to test if our pulesPerRevolution is accurate
Controller.prototype.moveOne = function (deviceName)
{
    var device = this.getDevice(deviceName)

    if (device !== undefined)
    {
        device.pulseMotor(device.name, device.pulsesPerRevolution);
    }

    return responses.successResponsePromise();
}

Controller.prototype.getExtents = function ()
{
    return new Promise(function (resolve, reject)
    {
        console.log(`calling getExtents...`);

        var response = responses.errorResponse();
        response.status.description = 'Could not resolve camera rig extents.';

        var x = this.getDevice('x')
        var y = this.getDevice('y')
        var z = this.getDevice('z')

        console.log(`extents devices ${x.name} ${y.name} ${z.name}`);

        if (x !== undefined && y !== undefined && z !== undefined)
        {
            response = responses.successResponse();
            response.extents = { x: x.linearLength, y: y.linearLength, z: z.linearLength };

            console.log(`extents devices ${x.linearLength} ${y.linearLength} ${z.linearLength}`);
        }

        console.log(`resolving response...`);
        resolve(response);
    }.bind(this));
}

Controller.prototype.pulseMotor = function (deviceName, pulseCount)
{
    var completedPulses = 0;

    var device = this.getDevice(deviceName)

    if (device !== undefined)
    {
        console.log(`pulsing device name: ${deviceName} will pulse: ${pulseCount} mode: ${device.mode}, current position: ${device.currentPosition}, max linear length: ${device.linearLength}, pulse sleep: ${device.pulseSleepMicroseconds}`);

        const startPosition = device.currentPosition;
        var pulseSleepMicroseconds = parseInt(device.pulseSleepMicroseconds);

        const revolutions = pulseCount / device.pulsesPerRevolution;
        const distance = revolutions * device.millimetersPerRevolution;
        pulseSleepMicroseconds *= 1/distance;
        pulseSleepMicroseconds = Math.floor(pulseSleepMicroseconds);

        //map the time to ease in/out
        for (var i = 0; i < pulseCount; i++)
        {
            if (device.currentPosition <= 0 && device.currentDirection === device.reverseDirection && device.mode !== Mode.Calibrate && device.mode !== Mode.CalibrateOverride && device.rotates !== true)
            {
                console.log(`pulseMotor validation error`, `Unable to move to a negative position.`);
                break;
            }
            else if (device.currentPosition >= device.linearLength && device.currentDirection === device.forwardDirection && device.mode !== Mode.Calibrate && device.mode !== Mode.CalibrateOverride && device.rotates !== true)
            {
                console.log(`pulseMotor validation error`, `Unable to move to a beyond max position.`);
                break;
            }
            else if (helper.isValid(device.limitIn) === true && device.limitIn.readSync() === 0 || device.mode === Mode.CalibrateOverride)    
            {
                device.pulseOut.writeSync(1);
                device.pulseOut.writeSync(0);
                sleep.usleep(pulseSleepMicroseconds);
                
                completedPulses++;

                //re-calc position at every step to validate that we are 
                //within range of acceptible positions
                const completedRevolutions = completedPulses / device.pulsesPerRevolution;
                const completedDistance = completedRevolutions * device.millimetersPerRevolution;
                if (device.currentDirection === device.forwardDirection)
                {
                    device.currentPosition = startPosition + completedDistance;
                }
                else
                {
                    device.currentPosition = startPosition - completedDistance;
                }
            }
            else
            {
                console.log('limit switch is on, stopping now');
                break;
            }
        }
        this.writeDeviceState(device);
    }

    console.log('pulsed motor and new position is', device.currentPosition);
    return completedPulses;
}

Controller.prototype.getDevice = function (name)
{
    console.log(`searching for device ${name}, device list is ${this.devices.length}`);

    var device = undefined;

    for (var i = 0; i < this.devices.length; i++)
    {
        var currentDevice = this.devices[i];
        console.log(`this device name is ${currentDevice.name}`);
        if (currentDevice.name === name)
        {
            console.log(`found the device, let's get out`);
            device = currentDevice
            break;
        }
    }

    return device;
}

Controller.prototype.save = function (state)
{

    return new Promise((resolve) =>
    {
        console.log(`saving state...`);

        const filename = `ui-states/ui-state-${state.timestamp}`;
        const fs = require('fs');
        const json = JSON.stringify(state);

        fs.writeFileSync(filename, json, 'utf8');
        resolve(responses.successResponse());
    });
}

Controller.prototype.getStates = function ()
{
    console.log(`get states`);
    return new Promise((resolve) =>
    {
        var states = [];

        const fs = require('fs');
        fs.readdirSync('ui-states').forEach(file =>
        {
            states.push(file);
        });

        console.log(`found ${states.length} states`);
        const response = responses.successResponse();
        response.states = states;
        resolve(response);
    });
}

Controller.prototype.getStateStores = function (state)
{
    return new Promise((resolve) =>
    {
        const fs = require('fs');
        const json = fs.readFileSync(`ui-states/${state}`, 'utf8');

        const response = responses.successResponse();
        response.stateStores = JSON.parse(json);;
        resolve(response);
    });
}

Controller.prototype.getCameraState = function ()
{
    return new Promise((resolve) =>
    {
        console.log(`get camera state`)
        const fs = require('fs');
        const x = fs.readFileSync(`camera-state/x`, 'utf8');
        const y = fs.readFileSync(`camera-state/y`, 'utf8');
        const z = fs.readFileSync(`camera-state/z`, 'utf8');
        const cr = fs.readFileSync(`camera-state/cameraRotate`, 'utf8');
        const ct = fs.readFileSync(`camera-state/cameraTilt`, 'utf8');

        console.log(`all files read`)

        const camera = `{"camera": {"x": ${x}, "y": ${y}, "z": ${z}, "cameraRotate": ${cr}, "cameraTilt": ${ct}}}`;
        console.log(`camera is ${camera}`);
        const response = responses.successResponse();
        response.camera = JSON.parse(camera);
        console.log(`resolve success...`);
        resolve(response);
    });
}

Controller.prototype.getDeviceState = function (deviceName)
{
    return new Promise((resolve) =>
    {
        const fs = require('fs');
        const deviceState = fs.readFileSync(`camera-state/${deviceName}`, 'utf8');

        const response = responses.successResponse();
        response.deviceState = JSON.parse(deviceState);
        resolve(response);
    });
}

Controller.prototype.setDeviceState = function (device)
{
    return new Promise((resolve) =>
    {
        console.log(`set device state`);

        //set in-memory info for device
        const device = this.getDevice(device.name);
        device.currentPosition = device.currentPosition;
        device.linearLength = device.linearLength;

        console.log(`write ${device.name} state to file`);

        //now to file...
        const fs = require('fs');
        fs.writeFileSync(`camera-state/${device.name}`, JSON.stringify(device), 'utf8');

        console.log(`camera state successfully written to file`);

        resolve(responses.successResponse());
    });
}

Controller.prototype.readDeviceState = function (deviceName)
{
    console.log(`read device state`);
    const fs = require('fs');

    const device = this.getDevice(deviceName);

    if (!fs.existsSync(`camera-state/${deviceName}`)) 
    {
        //initialize state
        this.writeDeviceState(device);
    }

    const json = fs.readFileSync(`camera-state/${deviceName}`, 'utf8');
    const deviceState = JSON.parse(json);

    console.log(`device state on disk is ${json}`);
    if (device !== undefined)
    {
        device.currentPosition = deviceState.currentPosition;
        device.linearLength = deviceState.linearLength;
    }

    console.log(`device state for ${deviceName} is set!`);
}

Controller.prototype.writeDeviceState = function (device)
{
    console.log(`write device state for ${device.name} to position ${device.currentPosition}, length ${device.linearLength}`);

    const fs = require('fs');
    fs.writeFileSync(`camera-state/${device.name}`, JSON.stringify(device), 'utf8');
    console.log(`camere file saved to disk`);
}

module.exports = new Controller;
