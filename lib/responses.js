'use strict';
var Promise = require("bluebird");

//Utilties for Communication Layer
module.exports.DESCRIPTION_OK = "OK";
module.exports.DESCRIPTION_ERROR = "An error has occurred.";

module.exports.CODE_SUCCESS = 1;
module.exports.CODE_ERROR = 2;
module.exports.CODE_EXCEPTION = 3;
module.exports.CODE_NOT_AUTHORIZED = 4;
module.exports.CODE_VALIDATION_ERROR = 5;
module.exports.CODE_USER_NOT_ASSIGNED = 6;

let Responses = function ()
{

}

Responses.prototype.successResponse = function ()
{
    let response =
    {
        status:
        {
            code: exports.CODE_SUCCESS,
            description: exports.DESCRIPTION_OK
        }
    }

    return response;
}

Responses.prototype.errorResponse = function ()
{
    let response =
    {
        status:
        {
            code: exports.CODE_ERROR,
            description: exports.DESCRIPTION_ERROR
        }
    }

    return response;
}

Responses.prototype.successResponsePromise = function ()
{
    return new Promise(function (resolve, reject)
    {
        let responses = new Responses();
        let response = responses.successResponse();
        resolve(response);
    });
}


Responses.prototype.errorResponsePromise = function (description = module.exports.DESCRIPTION_ERROR, code = module.exports.CODE_ERROR, stackTrace)
{
    return new Promise(function (resolve, reject)
    {
        let response =
        {
            status:
            {
                code: code,
                description: description,
                stackTrace: stackTrace
            }
        }
        reject(response);
    });
}

Responses.prototype.promiseRejectionResponse = function (reason)
{
    var response = reason;
    if (response.hasOwnProperty("status") === false)
    {
        let responses = new Responses();
        var response = responses.errorResponse();
        response.status.description = reason.toString();
        response.status.stackTrace = reason.stack;
    }
    return response;
}

module.exports = new Responses;