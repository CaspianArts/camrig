'use strict'

require('custom-env').env('local')

let express = require('express');
//allow app to be shared gobally so that
//certain resources can be global - such as the controller and DAL
let app = module.exports = express();

let controller = require("./lib/controller");
app.controller = controller;

let dal = require("./lib/dal")
app.dal = dal;

let bodyParser = require('body-parser');
let cors = require('cors');

app.use(bodyParser.json());
app.use(cors());


//initialize the controller...
controller.initialize({
    devices: [{
        name: 'x',
        pulsePin: process.env.X_PULSE_PIN,
        dirPin: process.env.X_DIR_PIN,
        limitPin: process.env.X_LIMIT_PIN,
        pulsesPerRevolution: process.env.X_PULSES_PER_REVOLUTION,
        millimetersPerRevolution: process.env.X_MILLIMETERS_PER_REVOLUTION,
        pulseSleepMicroseconds: process.env.X_PULSE_SLEEP_MICROSECONDS,
        forwardDirection: parseInt(process.env.X_FORWARD_DIRECTION),
        reverseDirection: parseInt(process.env.X_FORWARD_DIRECTION) === 1 ? 0 : 1
    },
    {
        name: 'y',
        pulsePin: process.env.Y_PULSE_PIN,
        dirPin: process.env.Y_DIR_PIN,
        limitPin: process.env.Y_LIMIT_PIN,
        pulsesPerRevolution: process.env.Y_PULSES_PER_REVOLUTION,
        millimetersPerRevolution: process.env.Y_MILLIMETERS_PER_REVOLUTION,
        pulseSleepMicroseconds: process.env.Y_PULSE_SLEEP_MICROSECONDS,
        forwardDirection: parseInt(process.env.Y_FORWARD_DIRECTION),
        reverseDirection: parseInt(process.env.Y_FORWARD_DIRECTION) === 1 ? 0 : 1
    },
    {
        name: 'z',
        pulsePin: process.env.Z_PULSE_PIN,
        dirPin: process.env.Z_DIR_PIN,
        limitPin: process.env.Z_LIMIT_PIN,
        pulsesPerRevolution: process.env.Z_PULSES_PER_REVOLUTION,
        millimetersPerRevolution: process.env.Z_MILLIMETERS_PER_REVOLUTION,
        pulseSleepMicroseconds: process.env.Z_PULSE_SLEEP_MICROSECONDS,
        forwardDirection: parseInt(process.env.Z_FORWARD_DIRECTION),
        reverseDirection: parseInt(process.env.Z_FORWARD_DIRECTION) === 1 ? 0 : 1
    },
    {
        name: 'cameraTilt',
        pulsePin: process.env.CT_PULSE_PIN,
        dirPin: process.env.CT_DIR_PIN,
        limitPin: process.env.CT_LIMIT_PIN,
        pulsesPerRevolution: process.env.CT_PULSES_PER_REVOLUTION,
        millimetersPerRevolution: process.env.CT_MILLIMETERS_PER_REVOLUTION,
        pulseSleepMicroseconds: process.env.CT_PULSE_SLEEP_MICROSECONDS,
        forwardDirection: parseInt(process.env.CT_FORWARD_DIRECTION),
        reverseDirection: parseInt(process.env.CT_FORWARD_DIRECTION) === 1 ? 0 : 1,
        rotates: true
    },
    {
        name: 'cameraRotate',
        pulsePin: process.env.CR_PULSE_PIN,
        dirPin: process.env.CR_DIR_PIN,
        limitPin: process.env.CR_LIMIT_PIN,
        pulsesPerRevolution: process.env.CR_PULSES_PER_REVOLUTION,
        millimetersPerRevolution: process.env.CR_MILLIMETERS_PER_REVOLUTION,
        pulseSleepMicroseconds: process.env.CR_PULSE_SLEEP_MICROSECONDS,
        forwardDirection: parseInt(process.env.CR_FORWARD_DIRECTION),
        reverseDirection: parseInt(process.env.CR_FORWARD_DIRECTION) === 1 ? 0 : 1,
        rotates: true
    }
    ]
});

//default endpoint
app.get('/', function (req, res)
{
    res.send('THM Media - Camera Rig Controller');
});

//create routes...
var controllerRouter = require('./web-api/controller-router');
app.use('/controller', controllerRouter);

app.listen(process.env.NODE_SERVER_PORT, function ()
{
    console.log(`Device Server is Listening on Port ${process.env.NODE_SERVER_PORT}`);
});
