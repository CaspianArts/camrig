var express = require('express');
var router = express.Router();

var responses = require("../lib/responses");

var uuidV4 = require('uuid/v4');

var controller = require("../lib/controller")

router.use(function (req, res, next)
{
    next();
});

router.get("/", function (req, res, next)
{
    res.send("THM Controller Router is functional")
});

router.post("/set/position", function (req, res, next)
{
    var position = req.body.position;
    var deviceName = req.body.deviceName;

    controller.setPosition(deviceName, position)
        .then(function (response)
        {
            res.send(response);
        }
        )
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/set/rotation", function (req, res, next)
{
    //in dregrees
    var rotation = req.body.rotation;
    var deviceName = req.body.deviceName;

    controller.setRotation(deviceName, rotation)
        .then(function (response)
        {
            res.send(response);
        }
        )
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/set/move", function (req, res, next)
{
    var millimeters = req.body.millimeters;
    var direction = req.body.direction
    var deviceName = req.body.deviceName;

    controller.move(deviceName, millimeters, direction)
        .then(function (response)
        {
            res.send(response);
        }
        )
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/set/calibrationMove", function (req, res, next)
{
    var millimeters = req.body.millimeters;
    var direction = req.body.direction
    var deviceName = req.body.deviceName;

    controller.calibrationMove(deviceName, millimeters, direction)
        .then(function (response)
        {
            res.send(response);
        }
        )
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/calibrate", function (req, res, next)
{
    var deviceName = req.body.deviceName;
 
    controller.calibrate(deviceName)
        .then(function (response)
        {
            res.send(response);
        }
        )
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/get/extents", function (req, res, next)
{
    controller.getExtents()
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/save", function (req, res, next)
{
    var state = req.body;
    controller.save(state)
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/get/states", function (req, res, next)
{
    controller.getStates()
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/get/state/stores", function (req, res, next)
{
    const state = req.body.state;

    controller.getStateStores(state)
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/get/camera/state", function (req, res, next)
{
    controller.getCameraState()
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/get/device/state", function (req, res, next)
{
    const deviceName = req.body.deviceName;

    controller.getDeviceState(deviceName)
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

router.post("/set/device/state", function (req, res, next)
{
    const device = req.body.device;

    console.log(`the device state is ${JSON.stringify(device)}`);

    controller.setDeviceState(device)
        .then(function (response)
        {
            res.send(response);
        })
        .catch(function (reason)
        {
            var response = responses.errorResponse();
            response.status.description = reason;
            response.status.stackTrace = new Error().stack;
            res.send(response);
        });
});

module.exports = router;
